#include <Servo.h>

Servo myservo;

const int micPin = A0;
const int servoPin = 9;
const int ledPin = 3;
const int switchPin = 4;
int sensorValue = 0;
int adj_value = 5;
int i;
float avg;
float count;

//-----Changable Variables------
int adjustment = 699;
int sampleSize = 100;
int servoMinDegree = 60;
int servoMaxDegree = 120;
int servoDegrees = 2; //How many steps per turn
int servoPosition = 60; //starting position of the servomotor
int thresholdHigh = 10;
int thresholdMedium = 5;
int thresholdLow = 3;
//------Time setup--------------
unsigned long currentMillis = 0;
unsigned long previous_Servo_Millis = 0;
unsigned long previous_Led_Millis = 0;
unsigned long previous_Sampler_Millis = 0;

//-------Time Intervals--------- In milliseconds
int SampleRate = 10;
int servoSlow = 30;
int servoMedium = 20;
int servoFast = 10;
int servoLow = 100;
int ledFrequency = 150;
int servoSpeed = servoSlow;
//---------------------------------
void setup() 
{
  Serial.begin(9600);
  myservo.attach(servoPin);
  myservo.write(servoPosition);
  pinMode(ledPin, OUTPUT);
  pinMode(switchPin, INPUT);
}

void loop()
{
  int bttn = digitalRead(switchPin);
  currentMillis = millis(); //Takes the current time
  if(bttn == 1)
  {
  	TakeSamples();
    servoMotion();
    setDelayTime();
    ledBlink();
  }
  else //fixes a bug where if the button is off for a long time it takes very long to catch up in current milliseconds.
  {
    previous_Servo_Millis = currentMillis;
    previous_Led_Millis = currentMillis;
    previous_Sampler_Millis = currentMillis;
  }
}

void TakeSamples()
{
  if (currentMillis - previous_Sampler_Millis >= SampleRate)
  {
    sensorValue = analogRead (micPin);
    //Serial.println(sensorValue);
    adj_value = abs(sensorValue-adjustment);
    //Serial.println (adjval, DEC);
    if(i >= sampleSize) 
    {
      avg = count/sampleSize;
      i = 0;
      Serial.print("\n Average = ");
      Serial.print(avg);
      Serial.print("\n");
      count = 0;
    }
    else
    {
      i += 1;
      count += adj_value;
      //Serial.println(count);
    }
    previous_Sampler_Millis += SampleRate;
  }
}
void servoMotion() 
{
  if (currentMillis - previous_Servo_Millis >= servoSpeed) 
  {
    
    previous_Servo_Millis += servoSpeed;
    
    servoPosition = servoPosition + servoDegrees;
    
    if ((servoPosition >= servoMaxDegree) || (servoPosition <= servoMinDegree))  
    {
      
      servoDegrees = - servoDegrees;
      
      servoPosition = servoPosition + servoDegrees; 
    }
    myservo.write(servoPosition);
  }
  
}
void setDelayTime() 
{
  servoSpeed = servoLow;
  if (avg >= thresholdHigh) {
    servoSpeed = servoFast;
  }
  else if (avg >= thresholdMedium) {
    servoSpeed = servoMedium;
  }
  else if (avg >= thresholdLow) {
  	servoSpeed = servoSlow;
  }
}
void ledBlink()
{
  if((servoMinDegree+10) <= servoPosition)
  {
    digitalWrite(ledPin, LOW);
  }
  else
  {
    digitalWrite(ledPin, HIGH);
  }
}
